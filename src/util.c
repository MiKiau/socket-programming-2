#include <stdio.h>
#include <string.h>
#include "constants.h"
#include "util.h"


int findOccurence(const char *buffer, const char *target)
{
    int bufferSize = (int)strlen(buffer);
    int targetSize = (int)strlen(target);
    if (bufferSize < targetSize) return -1;
    int result = -1;
    for (int index = 0; index < bufferSize - targetSize; index++)
    {
        for (int current = 0; current < targetSize; current++)
        {
            if (buffer[index + current] != target[current])
            {
                break;
            }
            if (current == targetSize - 1)
            {
                result = index;
            }
        }
        if (result >= 0)
        {
            break;
        }
    }
    return result;
}

char* copyFromString(char* dest, const char* src, const char* fromString, 
                     const int destSize)
{
    memset(dest, '\0', destSize);
    int index = findOccurence(src, fromString);
    if (index >= 0)
    {
        return memcpy(dest,
                      src + index + (int)strlen(fromString),
                      (int)strlen(src) - index);
    }
    return NULL;
}

int copyUntilSpecialCharOrMaxLen(char* dest, const char* src, 
                            const int MSG_LEN, const char* specialChars)
{
    if (specialChars == NULL || strlen(specialChars) == 0) 
    {
        return -1;
    }
    int lastIndex = 0;
    for (int i = 0; i < MSG_LEN; i++)
    {
        // Check if src[i] is among specialChars
        if (strchr(specialChars, src[i]) == NULL)
        {
            // If not, then saved it to destination string
            dest[lastIndex++] = src[i];
        }
        else
        {
            // If yes, then break it. We found our first special char
            lastIndex = i;
            break;
        }
    }
    return lastIndex;
}