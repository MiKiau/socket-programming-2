#include <string.h>             /* memcpy, memset */
#include <sys/socket.h>         /* socket, connect */
#include "dataExchangeFun.h"
#include "constants.h"
#include "checking.h"

extern char* receiveMessage(const int *sockfd, char *buffer, 
                            const int bufferSize)
{
    memset(buffer, '\0', bufferSize);
    int bytes = 0;
    for (int received = 0; received < bufferSize; received += bytes)
    {
        bytes = recv(*sockfd, buffer + received, bufferSize - received, 0);
        check(bytes, RECEIVING_MESSAGE_ERROR);
        if (bytes == 0) break;
    }
    return buffer;
}

static char* receiveSmallMessage(const int *sockfd, char *buffer, 
                                 const int bufferSize)
{
    memset(buffer, '\0', bufferSize);
    check(recv(*sockfd, buffer, bufferSize, 0),
          RECEIVING_MESSAGE_ERROR);
    return buffer;
}

extern int sendMessage(const int *sockfd, const char *buffer, 
                       const int messageSize)
{
    const int bytes = send(*sockfd, buffer, messageSize, 0);
    check(bytes, SENDING_MESSAGE_ERROR);
    return bytes;
}

extern char* decodeMessage(const int *sockfd, char *dest, const char *cipher)
{
    memset(dest, '\0', BIG_BUFFER);
    char smallPart[SMALL_BUFFER] = {0};
    int bytes = 0;
    for (int sent = 0; sent < (int)strlen(cipher); sent += bytes)
    {
        strncpy(smallPart, cipher + sent, MAX_MSG);
        bytes = sendMessage(sockfd, smallPart, MAX_MSG);
        receiveSmallMessage(sockfd, smallPart, SMALL_BUFFER);
        strcat(dest, smallPart);
    }
    return dest;
}