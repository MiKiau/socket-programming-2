#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <sys/socket.h> /* socket, connect */
#include <unistd.h>     /* close */
#include <arpa/inet.h>  /* htons */
#include "constants.h"
#include "checking.h"
#include "socketControl.h"

static struct sockaddr_in serverAddr = {0};

extern int createValidTCPSocket(void)
{
    const int sockfd = socket(COMM_DOMAIN, SOCK_STREAM, 0);
    check(sockfd, SOCKET_CREATION_ERROR);
    return sockfd;
}

static void initConnectionInfo(sa_family_t sinFamily,
                               const char *IP_address,
                               const uint16_t serverPort)
{
    serverAddr.sin_family = sinFamily;
    serverAddr.sin_addr.s_addr = inet_addr(IP_address);
    serverAddr.sin_port = htons(serverPort);
}

extern int connectToServer(const int *const sockfd, const int serverPort)
{
    initConnectionInfo(AF_INET, SERVER_IP, serverPort);
    return check(connect(*sockfd, (struct sockaddr *)&serverAddr,
                         (socklen_t)sizeof(struct sockaddr)),
                 SOCKET_CONNECTION_ERROR);
}

extern int closeSocket(const int *sockfd)
{
    int status = shutdown(*sockfd, SHUT_RDWR);
    if (status == 0)
    {
        status = close(*sockfd);
    }
    return status;
}