#include <stdlib.h> /* exit */
#include <stdio.h>  /* perror */

#include "checking.h"
#include "constants.h"

extern int check(const int target, const char *message)
{
    if (target < 0)
    {
        perror(message);
        exit(1);
        return VERIFICATION_FAILED;
    }
    return VERIFICATION_SUCCEEDED;
}