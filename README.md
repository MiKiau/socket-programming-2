# Socket programming 2

A simple client program for accessing HTTP web server. It features:
1. CMake building system;
2. cmocka unit testing;
3. Doxygen automatic documentation generation.

NOTE: this program sends all GET requests to a hardcoded server (IP and PORT). If you want to change that, then change values of SERVER_IP, SERVER_PORT in include/constants.h.

## Description

1. This program sends a HTTP GET request to a fixed server and a given page (index, cake, download). 
2. If the download page is chosen, then:
- this program first downloads the page (which contains a encoded text),
- then sends the text back to the same server, but different PORT, to decode the text.
- After that, this program receives decoded text and prints it in stdout.

# Requirements

1. Git;
2. CMake;
3. RECOMENDED: Linux (was not tested on other OS).

# Instalation

    $ git clone https://gitlab.com/MiKiau/socket-programming-2.git
    $ cd socket-programming-2
    $ mkdir build && cd build

If you want to only build the program, then:

    $ cmake ..
    $ make
    $ ./apps/CTF2_exe <index, cake or download>

If you want to also build tests, then:

    $ cmake -DENABLE_TESTS=true ..
    $ make && make test
    $ ./apps/CTF2_exe <index, cake or download>

If you want to also generate documentation, then:

    $ cmake -DGENERATE_DOCS=true ..
    $ make
    $ ./apps/CTF2_exe <index, cake or download>

If you want to also both generate documentations and build tests, then:

    $ cmake -DDO_ALL=true ..
    $ make && make test
    $ ./apps/CTF2_exe <index, cake or download>

# Testing enviroment

NOTE: this program was tested on Ubuntu Linux on VS Code.

# Changelog

All versions and their main added features.

## Version 0.3v

Added functionality for the program to decode a message. It does this by 
1. sending HTML request to a "download" page;
2. receiving the encoded text as a response;
3. sending the text in small packets to another port of the same server and receiving it's response;
4. after concatenating small packages of text into the whole text, it is printed in stdout.

## Version 0.2v

Added functionality for the program to send a HTML GET request, receive the response and print it to stdin.

## Version 0.1v

Added basic C template containing:
1. CMake structure;
2. Automated Doxygen documents generation;
3. Unit testing with cmocka;
4. A basic example.

