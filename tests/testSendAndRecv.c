#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include "cmocka.h"

#include "constants.h"
#include "dataExchangeFun.c"


int __wrap_check(const int target, const char *message)
{
    check_expected(target);
    check_expected_ptr(message);
    return mock();
}

int __wrap_recv(int __fd, void *__buf, size_t __n, int __flags)
{
    check_expected(__fd);
    check_expected_ptr(__buf);
    check_expected(__n);
    check_expected(__flags);
    return mock();
}

static void receiveMessage_returnsBuffer(void **state)
{
    (void)state;
}

static void receiveMessage_successCase(void **state)
{
    (void)state;

    // // Any sockfd, does not make any difference
    // const int sockfd = 50;
    // const char message[BIG_BUFFER] = {0};
    // const int bytesReceived = 10;

    // expect_value(__wrap_recv, __fd, sockfd);
    // expect_value(__wrap_recv, __buf, message);
    // expect_value(__wrap_recv, __n, BIG_BUFFER);
    // expect_value(__wrap_recv, __flags, 0);
    // will_return(__wrap_recv, bytesReceived);

    // expect_value(__wrap_check, target, bytesReceived);
    // expect_value(__wrap_check, message, RECEIVING_MESSAGE_ERROR);
    // will_return(__wrap_check, VERIFICATION_SUCCEEDED);

    // assert_ptr_equal(receiveMessage(&sockfd, message, BIG_BUFFER), message);
}

static void receiveMessage_failureCase(void **state)
{
    (void)state;
}

ssize_t __wrap_send(int __fd, const void *__buf, size_t __n, int __flags)
{
    check_expected(__fd);
    check_expected_ptr(__buf);
    check_expected(__n);
    check_expected(__flags);
    return mock();
}

static void sendMessage_returnsTheNumberOfBytesSent(void **state)
{
    (void)state;

    // Any sockfd, does not make any difference
    const int sockfd = 50;
    // Sent 0 bytes
    const int bytes = 0;

    const char *message = "";
    const int messageSize = (int)strlen(message);

    expect_value(__wrap_send, __fd, sockfd);
    expect_value(__wrap_send, __buf, message);
    expect_value(__wrap_send, __n, messageSize);
    expect_value(__wrap_send, __flags, 0);
    will_return(__wrap_send, bytes);

    expect_value(__wrap_check, target, bytes);
    expect_value(__wrap_check, message, SENDING_MESSAGE_ERROR);
    will_return(__wrap_check, VERIFICATION_SUCCEEDED);

    assert_int_equal(sendMessage(&sockfd, message, messageSize),
                     bytes);
}

static void sendMessage_successCase(void **state)
{
    (void)state;

    // Any sockfd, does not make any difference
    const int sockfd = 50;

    const char *message = "TESTING MESSAGE";
    const int messageSize = (int)strlen(message);
    const int bytes = messageSize;

    expect_value(__wrap_send, __fd, sockfd);
    expect_value(__wrap_send, __buf, message);
    expect_value(__wrap_send, __n, messageSize);
    expect_value(__wrap_send, __flags, 0);
    will_return(__wrap_send, bytes);

    expect_value(__wrap_check, target, bytes);
    expect_value(__wrap_check, message, SENDING_MESSAGE_ERROR);
    will_return(__wrap_check, VERIFICATION_SUCCEEDED);

    assert_int_equal(sendMessage(&sockfd, message, messageSize),
                     bytes);
}

static void sendMessage_failureCase(void **state)
{
    (void)state;

    // Any sockfd, does not make any difference
    const int sockfd = 50;

    const char *message = "TESTING MESSAGE";
    const int messageSize = (int)strlen(message);
    const int bytes = -1;

    expect_value(__wrap_send, __fd, sockfd);
    expect_value(__wrap_send, __buf, message);
    expect_value(__wrap_send, __n, messageSize);
    expect_value(__wrap_send, __flags, 0);
    will_return(__wrap_send, bytes);

    expect_value(__wrap_check, target, bytes);
    expect_value(__wrap_check, message, SENDING_MESSAGE_ERROR);
    will_return(__wrap_check, VERIFICATION_FAILED);

    assert_int_equal(sendMessage(&sockfd, message, messageSize),
                     bytes);
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(receiveMessage_returnsBuffer),
        cmocka_unit_test(receiveMessage_successCase),
        cmocka_unit_test(receiveMessage_failureCase),
        cmocka_unit_test(sendMessage_returnsTheNumberOfBytesSent),
        cmocka_unit_test(sendMessage_successCase),
        cmocka_unit_test(sendMessage_failureCase),

    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}