#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include "cmocka.h"

#include "constants.h"
#include "util.c"

typedef struct vector3s
{
    char src[BIG_BUFFER];
    char copyFrom[SMALL_BUFFER];
    char expected[SMALL_BUFFER];
} vector3s_t;

typedef struct vector1s1i
{
    char src[BIG_BUFFER];
    int sourceCount;
} vector1s1i_t;

static void findOccurence_substringIsBiggerThanSourceString(void **state)
{
    (void)state;
    // Substring is bigger than the source string
    assert_int_equal(findOccurence("a", "asda"), -1);
}

static void findOccurence_sourceStringDoesNotHaveSubstring(void **state)
{
    (void)state;
    // Source string does not contain the substring
    assert_int_equal(findOccurence("PORASAS", "Z"), -1);
}
static void findOccurence_checkCorrectness(void **state)
{
    (void)state;
    assert_int_equal(findOccurence("PORASAS", "ASA"), 3);
    assert_int_equal(findOccurence("AKASASDASDADASDfsSDF", "fs"), 15);
}

static void copyFromString_returnsDestination(void **state)
{
    (void)state;
    char dest[SMALL_BUFFER] = {0};
    // Return destination is the same as the dest string
    assert_string_equal(copyFromString(dest, "KARAS", "AR", SMALL_BUFFER), 
                        dest);
}

static void copyFromString_returnsNullIfNoSubstringIsFound(void **state)
{
    (void)state;
    char dest[SMALL_BUFFER] = {0};
    // Return null if no substring is found
    assert_ptr_equal(copyFromString(dest, "ASDASD", "5", SMALL_BUFFER), NULL);
}

static void copyFromString_checkCorrectness(void **state)
{
    (void)state;
    const vector3s_t vectors[] = {
        {"12315asASdafasf/asdaf/", "/", "asdaf/"},
        {"5165164819952**asdasdafsf", "**", "asdasdafsf"},
        {"KAIPMANSASFAGASFSAGDHGFBFF", "SAG", "DHGFBFF"}
    };

    const int until = (int)sizeof(vectors) / (int)sizeof(vector3s_t);
    for (int i = 0; i < until; i++)
    {
        const vector3s_t *vector = &vectors[i];
        char dest[BIG_BUFFER] = {0};
        assert_string_equal(copyFromString(dest, vector->src, 
                                           vector->copyFrom, BIG_BUFFER),
                            vector->expected);
    }
}

static void copyUntilSpecialCharOrMaxLen_substringIsEmpty(void **state)
{
    (void)state;
    char dest[BIG_BUFFER] = {0};
    assert_int_equal(copyUntilSpecialCharOrMaxLen(dest, "ASdsfdsdgfdg", 
                                                  MAX_MSG, NULL),
                    -1);
}

static void copyUntilSpecialCharOrMaxLen_checkCorrectness(void **state)
{
    (void)state;
    const vector1s1i_t vectors[] = {
        {"ASdsfdsdgfdg", (int)strlen("ASdsfdsdgfdg")},
        {"ASKJ;ASDKFAO'", (int)strlen("ASKJ")},
        {"156fsdfdf..", (int)strlen("156fsdfdf")},
        {"..", 0}
    };
    const char *chars = "!,\".-\'’;";

    const int until = (int)sizeof(vectors) / (int)sizeof(vector1s1i_t);
    for (int i = 0; i < until; i++)
    {
        const vector1s1i_t *vector = &vectors[i];
        char dest[BIG_BUFFER] = {0};
        assert_int_equal(
            copyUntilSpecialCharOrMaxLen(dest, vector->src, MAX_MSG, chars),
            (MAX_MSG < vector->sourceCount) ? MAX_MSG : vector->sourceCount
        );
    }
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(findOccurence_substringIsBiggerThanSourceString),
        cmocka_unit_test(findOccurence_sourceStringDoesNotHaveSubstring),
        cmocka_unit_test(findOccurence_checkCorrectness),
        cmocka_unit_test(copyFromString_returnsDestination),
        cmocka_unit_test(copyFromString_returnsNullIfNoSubstringIsFound),
        cmocka_unit_test(copyFromString_checkCorrectness),
        cmocka_unit_test(copyUntilSpecialCharOrMaxLen_substringIsEmpty),
        cmocka_unit_test(copyUntilSpecialCharOrMaxLen_checkCorrectness),
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}