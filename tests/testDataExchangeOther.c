#include <string.h>
#include <stdio.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include "cmocka.h"

#include "constants.h"
#include "dataExchangeFun.c"

char *__wrap_receiveMessage(const int *sockfd, char *buffer,
                            const int bufferSize)
{
    check_expected_ptr(sockfd);
    check_expected_ptr(buffer);
    check_expected(bufferSize);
    return mock_ptr_type(char *);
}

char *__wrap_sendMessage(const int *sockfd, char *buffer,
                         const int messageSize)
{
    check_expected_ptr(sockfd);
    check_expected_ptr(buffer);
    check_expected(messageSize);
    return mock_ptr_type(char *);
}

void test_decodeMessage(void **state)
{
    (void)state;
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_decodeMessage)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}