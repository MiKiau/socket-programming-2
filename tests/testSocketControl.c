#include <string.h>
#include <stdio.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include "cmocka.h"

#include "constants.h"
#include "socketControl.c"


int __wrap_socket(int __domain, int __type, int __protocol)
{
    check_expected(__domain);
    check_expected(__type);
    check_expected(__protocol);

    return mock_type(int);
}

int __wrap_check(const int target, const char* message)
{
    check_expected(target);
    check_expected_ptr(message);

    return (target < 0) ? 0 : 1;
}

static void createValidTCPSocket_successCase(void **state)
{
    (void)state;

    // socket function expects creation of a TCP/IP STREAM type socket
    expect_value(__wrap_socket, __domain, COMM_DOMAIN);
    expect_value(__wrap_socket, __type, SOCK_STREAM);
    expect_value(__wrap_socket, __protocol, 0);
    // The socket creation succeeded, so it returns positive number
    const int sockfd = 2;
    will_return(__wrap_socket, sockfd);

    expect_value(__wrap_check, target, sockfd);
    expect_string(__wrap_check, message, SOCKET_CREATION_ERROR);

    assert_int_equal(createValidTCPSocket(), sockfd);
}

static void createValidTCPSocket_failureCase(void **state)
{
    (void)state;

    expect_value(__wrap_socket, __domain, COMM_DOMAIN);
    expect_value(__wrap_socket, __type, SOCK_STREAM);
    expect_value(__wrap_socket, __protocol, 0);
    // The OS failed to create a socket, so it returns -1
    const int sockfd = -1;
    will_return(__wrap_socket, sockfd);

    expect_value(__wrap_check, target, sockfd);
    expect_string(__wrap_check, message, SOCKET_CREATION_ERROR);

    assert_int_equal(createValidTCPSocket(), sockfd);
}

int __wrap_connect(int __fd, const struct sockaddr *__addr, socklen_t __len)
{
    check_expected(__fd);
    check_expected_ptr(__addr);
    check_expected(__len);
    return mock();
}

static void connectToServer_successCase(void **state)
{
    (void)state;
    
    // Any port, in this test this value does not create any difference
    const int serverPort = 5555;
    const int sockfd = 50;
    expect_value(__wrap_connect, __fd, sockfd);
    expect_value(__wrap_connect, __addr, (struct sockaddr *)&serverAddr);
    expect_value(__wrap_connect, __len, (socklen_t)sizeof(struct sockaddr));

    will_return(__wrap_connect, 0);

    expect_value(__wrap_check, target, 0);
    expect_string(__wrap_check, message, SOCKET_CONNECTION_ERROR);

    assert_int_equal(connectToServer(&sockfd, serverPort), 1);
}

static void connectToServer_failureCase(void **state)
{
    (void)state;

    // Any port, in this test this value does not create any difference
    const int serverPort = 5555;
    const int sockfd = 50;
    expect_value(__wrap_connect, __fd, sockfd);
    expect_value(__wrap_connect, __addr, (struct sockaddr *)&serverAddr);
    expect_value(__wrap_connect, __len, (socklen_t)sizeof(struct sockaddr));

    will_return(__wrap_connect, -1);

    expect_value(__wrap_check, target, -1);
    expect_string(__wrap_check, message, SOCKET_CONNECTION_ERROR);

    assert_int_equal(connectToServer(&sockfd, serverPort), 0);
}

int __wrap_shutdown(int __fd, int __how)
{
    check_expected(__fd);
    check_expected(__how);
    return mock();
}

int __wrap_close(int __fd)
{
    check_expected(__fd);
    return mock();
}

static void closeSocket_successCase(void **state)
{
    (void)state;

    const int sockfd = 50;

    expect_value(__wrap_shutdown, __fd, sockfd);
    expect_value(__wrap_shutdown, __how, SHUT_RDWR);
    // Shutdown succeeded
    const int returnValue = 0;
    will_return(__wrap_shutdown, returnValue);

    expect_value(__wrap_close, __fd, sockfd);
    will_return(__wrap_close, returnValue);

    assert_int_equal(closeSocket(&sockfd), returnValue);
}

static void closeSocket_failureCase(void **state)
{
    (void)state;

    const int sockfd = 50;

    expect_value(__wrap_shutdown, __fd, sockfd);
    expect_value(__wrap_shutdown, __how, SHUT_RDWR);
    // Failed to shutdown
    const int returnValue = -1;
    will_return(__wrap_shutdown, returnValue);

    assert_int_equal(closeSocket(&sockfd), returnValue);
}

int main(void)
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(createValidTCPSocket_successCase),
        cmocka_unit_test(createValidTCPSocket_failureCase),
        cmocka_unit_test(connectToServer_successCase),
        cmocka_unit_test(connectToServer_failureCase),
        cmocka_unit_test(closeSocket_successCase),
        cmocka_unit_test(closeSocket_failureCase)
    };
    return cmocka_run_group_tests(tests, NULL, NULL);
}