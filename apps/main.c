#include <stdio.h>
#include <string.h>
#include "constants.h"
#include "socketControl.h"
#include "dataExchangeFun.h"
#include "util.h"

int main(int argc, char *argv[])
{
    if (argc != 2) 
    {
        puts("Format: ./CTF2_run <index, download, cake>");
        return 1;
    }

    char buffer[BIG_BUFFER] = {0};
    if (strcmp(argv[1], "index") == 0)
    {
        sprintf(buffer, "GET / HTTP/1.1\r\n\r\n");
    }
    else if (strcmp(argv[1], "download") == 0 || strcmp(argv[1], "cake") == 0)
    {
        sprintf(buffer, "GET /%s HTTP/1.1\r\n\r\n", argv[1]);
    }
    else
    {
        printf("%s - incorrect word. Choose from: index, download, cake.\n", argv[1]);
        return 1;
    }

    int sockfd = createValidTCPSocket();
    connectToServer(&sockfd, SERVER_PORT);

    printf("Sending request for %s...\n", argv[1]);
    sendMessage(&sockfd, buffer, strlen(buffer));

    puts("Receiving response...\n");
    puts(receiveMessage(&sockfd, buffer, BIG_BUFFER));

    closeSocket(&sockfd);

    if (strcmp(argv[1], "download") == 0)
    {
        printf("\nDecipthering the text by comunicating with server ");
        printf("of source IP and 5555 port...\n");

        sockfd = createValidTCPSocket();
        connectToServer(&sockfd, DECIPHER_PORT);

        char cipher[BIG_BUFFER] = {0};
        copyFromString(cipher, buffer, "keep-alive", BIG_BUFFER);

        printf("Received message:");
        printf("%s", decodeMessage(&sockfd, buffer, cipher));

        closeSocket(&sockfd);
    }
    puts("\nProgram ended successfully.");
    return 0;
}