#ifndef UTIL_H
#define UTIL_H

#ifdef __cplusplus
extern "C"
{
#endif


int findOccurence(const char *buffer, const char *target);
char *copyFromString(char *dest, const char *src, const char *fromString,
                     const int destSize);
int copyUntilSpecialCharOrMaxLen(char *dest, const char *src,
                                 const int MSG_LEN, const char *specialChars);

#ifdef __cplusplus
}
#endif

#endif // UTIL_H