#ifndef DATAEXCHANGEFUN_H
#define DATAEXCHANGEFUN_H

#ifdef __cplusplus
extern "C"
{
#endif

extern char* receiveMessage(const int *sockfd, char *buffer,
                            const int bufferSize);
extern int sendMessage(const int *sockfd, const char *buffer,
                       const int messageSize);
extern char* decodeMessage(const int *sockfd, char *dest, const char *cipher);

#ifdef __cplusplus
}
#endif

#endif // DATAEXCHANGEFUN_H