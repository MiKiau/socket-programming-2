#ifndef CHECKING_H
#define CHECKING_H

#ifdef __cplusplus
extern "C"
{
#endif

extern int check(const int target, const char *message);

#ifdef __cplusplus
}
#endif

#endif // CHECKING_H