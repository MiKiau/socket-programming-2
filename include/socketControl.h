#ifndef SOCKETCONTROL_H
#define SOCKETCONTROL_H

#ifdef __cplusplus
extern "C"
{
#endif

extern int createValidTCPSocket(void);
extern int connectToServer(const int *const sockfd, const int serverPort);
extern int closeSocket(const int *sockfd);

#ifdef __cplusplus
}
#endif

#endif // SOCKETCONTROL_H