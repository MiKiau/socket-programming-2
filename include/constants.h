#ifndef CONSTANTS_H
#define CONSTANTS_H

#define COMM_DOMAIN             AF_INET
#define SERVER_IP               "78.56.77.230"
#define SERVER_PORT             4040
#define DECIPHER_PORT           5555

#define BIG_BUFFER              5000
#define SMALL_BUFFER            30
#define MAX_MSG                 8

#define SOCKET_CREATION_ERROR   "Socket creation failed"
#define SOCKET_CONNECTION_ERROR "Connection failed"
#define RECEIVING_MESSAGE_ERROR "Receive failed"
#define SENDING_MESSAGE_ERROR   "Sending message failed"

#define VERIFICATION_SUCCEEDED  1
#define VERIFICATION_FAILED     0

#endif // CONSTANTS_H